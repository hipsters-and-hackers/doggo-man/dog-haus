import unittest
import requests

class TestEndPoints(unittest.TestCase):
    def test_not_found(self):
        response = requests.get('http://docker:5000/')
        self.assertFalse(response.ok)
    
    def test_random_doggo_status(self):
        response = requests.get('http://docker:5000/doggo-info/random')
        self.assertEqual(response.status_code, 200)
    
    def test_random_data(self):
        response = requests.get('http://docker:5000/doggo-info/random')

        doggo_test = {
            "breed": "Akita",
            "img_path": "/somewhere/",
            "personality": "Suplado",
            "ideal_owners": "akind",
            "care_reqs": "love",
            "fun_fact": "it is blah"
        }

        self.assertDictEqual(response.json(), doggo_test)


if __name__ == "__main__":
    unittest.main()