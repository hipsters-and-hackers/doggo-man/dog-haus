"""
test module for doggo
"""
import unittest

import sys
sys.path.append('modules')
import doggo

class TestDoggo(unittest.TestCase):
    def test_random(self):
        doggo_test = {
            "breed": "Akita",
            "img_path": "/somewhere/",
            "personality": "Suplado",
            "ideal_owners": "akind",
            "care_reqs": "love",
            "fun_fact": "it is blah"
        }
    
        self.assertDictEqual(doggo_test, doggo.random())

if __name__ == "__main__":
    unittest.main()
        