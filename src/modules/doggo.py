"""
This is the people module and supports all the ReST actions for the
doggo collection
"""
from datetime import datetime

# 3rd party modules
from flask import make_response, abort

# Data to serve with our API
DOGGO = {
    "breed": "Akita",
    "img_path": "/somewhere/",
    "personality": "Suplado",
    "ideal_owners": "akind",
    "care_reqs": "love",
    "fun_fact": "it is blah"
}


# Create a handler for our read (GET) doggo
def random():
    """
    This function responds to a request for /doggo-info/random

    # Create the list of doggo from our data
    """
    return DOGGO