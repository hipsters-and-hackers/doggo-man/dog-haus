Doggo man backend

```
docker build . -t dog-haus
docker container run -p 5000:5000 dog-haus

dev:
docker container run -p 5000:5000 -v ${pwd}:/src/ dog-haus
```


on your browser:


to get random doggo-info:

`http://localhost:5000/doggo-info/random`

endpoints documentation:

`http://localhost:5000/doggo-info/ui`